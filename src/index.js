import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';

const month = [
  'Январь',
  'Февраль' ,
  'Март', 
  'Апрель', 
  'Май', 
  'Июнь',
  'Июль', 
  'Август', 
  'Сентябрь', 
  'Октябрь', 
  'Ноябрь', 
  'Декабрь'
]

const List = function() {
  return (
    <ul>
      {month.map((item, index) => {
        return <li>{item}</li>
      })}
    </ul>
  )
}

ReactDOM.render(<List/>, document.getElementById('root'))

